from django.conf.urls import url
from digbata import views

urlpatterns = [
    url(r'^$', views.index_page, name='project_list'),
    url(r'^(?P<page>\w{0,50})/$', views.index_page),
    url(r'^project/(?P<source_id>[0-9]+)/(?P<project_id>[0-9]+)$', views.project, name='project_view'),
]
