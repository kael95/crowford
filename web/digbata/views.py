from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from digbata.models import ProjectSummary
from digbata.models import ProjectRelated
from digbata.models import Perk
from digbata.models import AuthorProject
from digbata.models import Project
from digbata.models import Author


def index(request):
    return index_page(request)

def index_page(request):
    page = request.GET.get('page')

    # Default filter
    src_val = [1,2,3]
    status_val = [True, False]
    success_val = [True, False]
    # Put values
    if (request.GET.getlist('source') != []):
        src_val = list(map(int, request.GET.getlist('source')))
    if (request.GET.getlist('status') != []):
        status_val = list(map(bool, request.GET.getlist('status')))
    if (request.GET.getlist('prediction') != []):
        success_val = list(map(bool, request.GET.getlist('prediction')))
    option_string = ''
    for x in src_val:
        option_string = option_string + '&source={0}'.format(x)
    for x in status_val:
        option_string = option_string + '&status={0}'.format('True' if x else '')
    for x in success_val:
        option_string = option_string + '&prediction={0}'.format('True' if x else '')
    options = {
        'sources'     : src_val,
        'statuses'    : status_val,
        'successes'   : success_val,
        'params'      : option_string,
    }

    # Categ and Location filter
    categ_val = request.GET.get('category')
    if categ_val is not None and categ_val == 'All':
        categ_val = None
    locat_val = request.GET.get('location')
    if locat_val is not None and locat_val == 'All':
        locat_val = None
    
    if (categ_val is not None or locat_val is not None):
        if (categ_val is not None and locat_val is not None):
            all_summaries = ProjectSummary.objects.filter(score_success__in=success_val, id_source__in=src_val, is_finished__in=status_val, category=categ_val, location=locat_val)
        elif (categ_val is not None):
            all_summaries = ProjectSummary.objects.filter(score_success__in=success_val, id_source__in=src_val, is_finished__in=status_val, category=categ_val)
        else :
            all_summaries = ProjectSummary.objects.filter(score_success__in=success_val, id_source__in=src_val, is_finished__in=status_val, location=locat_val)
    else :
        all_summaries = ProjectSummary.objects.filter(score_success__in=success_val, id_source__in=src_val, is_finished__in=status_val)

    #all_summaires = ProjectSummary.objects.all()

    paginator = Paginator(all_summaries, 20) # Show 20 contacts per page
  
    try:
        all_summaries = paginator.page(page)
    except PageNotAnInteger:
        all_summaries = paginator.page(1)
    except EmptyPage:
        all_summaries = paginator.page(paginator.num_pages)

    projs_ids = [p.id_project_id for p in all_summaries]
    all_projects = Project.objects.filter(id_project__in = projs_ids)
    
    all_projects = dict([(obj.id_project, obj) for obj in all_projects])
    sorted_projs = [all_projects[id] for id in projs_ids]

    tuple_projects = zip(all_summaries, sorted_projs)
    context = {
      'options'         : options,
      'pagenum'         : page,
      'iternum'         : range(len(all_projects)),
      'summaries'       : all_summaries,
      'projects'        : sorted_projs,
      'tuple_projects'  : tuple_projects,
    }
    return render(request, 'digbata/project_list/index.html', context)

# http://localhost:8000/crowford/project/1/2052607475
def project(request, source_id, project_id):
    summary = ProjectSummary.objects.get(id_source=source_id, id_project=project_id)
    authors = Author.objects.filter(authorproject__id_project=project_id, authorproject__id_source=source_id)
    project = Project.objects.get(id_source=source_id, id_project=project_id)
    perks = Perk.objects.filter(id_source=source_id, id_project=project_id)

    related_projs = ProjectRelated.objects.filter(id_source=source_id, id_project=project_id)

    context = {
        'summary' : summary,
        'project' : project,
        'authors' : authors,
        'perks'   : perks,
        'related' : related_projs,
    }

    return render(request, 'digbata/project_view/index.html', {'context': context})

def project_list(request):
	return render(request, 'digbata/project_list/index.html', {})
