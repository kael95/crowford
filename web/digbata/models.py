# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Author(models.Model):
    id_author = models.IntegerField(primary_key=True)
    id_source = models.IntegerField(primary_key=True)
    url_profile = models.CharField(max_length=250, blank=True, null=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    nb_suc_campaigns_sup = models.IntegerField(blank=True, null=True)
    nb_pen_campaigns_sup = models.IntegerField(blank=True, null=True)
    nb_uns_campaigns_sup = models.IntegerField(blank=True, null=True)
    nb_suc_campaigns_cre = models.IntegerField(blank=True, null=True)
    nb_pen_campaigns_cre = models.IntegerField(blank=True, null=True)
    nb_uns_campaigns_cre = models.IntegerField(blank=True, null=True)
    nb_campaigns_sup = models.IntegerField(blank=True, null=True)
    nb_campaigns_cre = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'author'
        unique_together = (('id_author', 'id_source'),)


class AuthorProject(models.Model):
    id_author = models.ForeignKey(Author, models.DO_NOTHING, db_column='id_author', primary_key=True)
    id_project = models.ForeignKey('Project', models.DO_NOTHING, db_column='id_project', primary_key=True)
    id_source = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'author_project'
        unique_together = (('id_author', 'id_project', 'id_source'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Perk(models.Model):
    id_perk = models.IntegerField(primary_key=True)
    id_project = models.ForeignKey('Project', models.DO_NOTHING, db_column='id_project', primary_key=True)
    id_source = models.IntegerField(primary_key=True)
    price = models.IntegerField(blank=True, null=True)
    nb_sold = models.IntegerField(blank=True, null=True)
    nb_max = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'perk'
        unique_together = (('id_perk', 'id_project', 'id_source'),)


class Project(models.Model):
    id_project = models.IntegerField(primary_key=True)
    id_source = models.IntegerField(primary_key=True)
    category = models.CharField(max_length=100, blank=True, null=True)
    title = models.CharField(max_length=250, blank=True, null=True)
    url = models.CharField(max_length=250, blank=True, null=True)
    date_end = models.CharField(max_length=250, blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    fulldesc = models.TextField(blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    currency = models.CharField(max_length=3, blank=True, null=True)
    money_percent = models.IntegerField(blank=True, null=True)
    money_goal = models.IntegerField(blank=True, null=True)
    money_pledged = models.IntegerField(blank=True, null=True)
    nb_characters = models.IntegerField(blank=True, null=True)
    nb_images = models.IntegerField(blank=True, null=True)
    nb_supporters = models.IntegerField(blank=True, null=True)
    nb_updates = models.IntegerField(blank=True, null=True)
    nb_comments = models.IntegerField(blank=True, null=True)
    finished = models.NullBooleanField()
    dt_creation = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'project'
        unique_together = (('id_project', 'id_source'),)


class ProjectRelated(models.Model):
    id_project = models.ForeignKey(Project, models.DO_NOTHING, db_column='id_project', primary_key=True)
    id_source = models.IntegerField(primary_key=True)
    id_project_related = models.IntegerField(primary_key=True)
    id_source_related = models.IntegerField(primary_key=True)
    title_related = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'project_related'
        unique_together = (('id_project', 'id_source', 'id_project_related', 'id_source_related'),)


class ProjectSummary(models.Model):
    id_project = models.ForeignKey(Project, models.DO_NOTHING, db_column='id_project')
    id_source = models.IntegerField(primary_key=True)
    nb_chr_title = models.IntegerField(blank=True, null=True)
    nb_chr_summary = models.IntegerField(blank=True, null=True)
    nb_chr_description = models.IntegerField(blank=True, null=True)
    nb_authors = models.IntegerField(blank=True, null=True)
    avg_campaigns_sup = models.IntegerField(blank=True, null=True)
    avg_campaigns_cre = models.IntegerField(blank=True, null=True)
    nb_perks = models.IntegerField(blank=True, null=True)
    avg_price = models.IntegerField(blank=True, null=True)
    avg_nb_available = models.IntegerField(blank=True, null=True)
    avg_nb_sold = models.IntegerField(blank=True, null=True)
    score_success = models.NullBooleanField()
    is_finished = models.NullBooleanField()
    date_creation = models.CharField(max_length=250, blank=True, null=True)
    location = models.CharField(max_length=100, blank=True, null=True)
    category = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'project_summary'
        unique_together = (('id_project', 'id_source'),)
