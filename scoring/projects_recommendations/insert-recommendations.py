import csv
import json
import psycopg2
import os
import sys
import re
import time
import numpy as np

if len(sys.argv) != 2:
    print 'Usage: ', sys.argv[0], ' <db_password>'
    sys.exit(1)
db_password = sys.argv[1]

# ----- DATA LOADING -----
recommendations = []
filename = "datasets/csv/projects_related_{0}.csv"
for i in range(5000, 40000, 5000):
	with open(filename.format(i)) as f:
		reader = csv.DictReader(f, delimiter=',')
		for row in reader:
			recommendations.append(row)

# ----- DATABASE INSERTS -----
# Useful functions
def strToInt(str):
    res = re.sub(r'([0-9]+).?([0-9]*)','\\1',str)
    return int(res) if res else None

# Initialize connection
connection = psycopg2.connect(
    host='localhost', port=5432,
    database='crowford', user='kael',
    password=db_password
)
cursor = connection.cursor()

# Delete Data
delQuery = "DELETE FROM project_related"
start = time.clock()
cursor.execute(delQuery)
connection.commit()
end = time.clock()
print("Execution time of delete queries: {0}").format(end - start)

# SQL Queries
insProjectRelatedQuery = '''
INSERT INTO project_related(
    id_project, id_source, id_project_related, id_source_related,
	title_related, url_related, coef_similarity
) VALUES {0} ON CONFLICT DO NOTHING'''
insProjectRelatedValues = []

# Loop on recommendations
errors = {}
i = 1
start = time.clock()
len_data = len(recommendations)
nb_elts_skipped=0
for elt in recommendations:
    try:
        # Insert Project Related Values
        values = (
            elt['id_project'], elt['id_source'], elt['id_project_related'], elt['id_source_related'],
            elt['title_related'], elt['url_related'], elt['coef_similarity']
        )
        insProjectRelatedValues.append(values)
                
    except Exception as e:
        errors[elt['id_project']] = e

    # Avoid consuming a lot of memory by executing queries every 20 projects
    if ((i % 20) == 0 or i == len_data):
        # Project
        if insProjectRelatedValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insProjectRelatedValues)
            cursor.execute(insProjectRelatedQuery.format(str_values))

        # reset
        insProjectRelatedValues = []

        end = time.clock()
        timedif = ((end - start) % 180)
        if ((timedif >=0 and timedif <= 20) or (timedif >= 160 and timedif < 180)):
            print("Time: {0} | Elements: {1} | Total: {2} | Percentage: {3}").format(
                end - start, i, len_data, i / len_data * 100
            )

    i += 1
connection.commit()
connection.close()
end = time.clock()
nb_errors=len(errors)
print("Execution time of processing + insert queries: {0}").format(end - start)
print("Number of errors: {0}").format(nb_errors)
print("Number of elements skipped: {0}").format(nb_elts_skipped)
print("Number of elements successfully inserted: {0}").format(len_data - nb_errors - nb_elts_skipped)

for k,v in errors.items():
    print("product id: ", k, " v: ", v)

