from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from nltk.stem.porter import PorterStemmer
import nltk, string

def stemtkn(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

def tokenize(text):
    stemmer = PorterStemmer()
    tokens = nltk.word_tokenize(text)
    stems = stemtkn(tokens, stemmer)
    return stems

def get_clean_data(data):
    tokens = []
    for elt in data:
        text = (elt.lower()).translate(None, string.punctuation)
        tokens.append(text)
    return tokens

def get_topx_related_projects(vector_project, tfidf, topx=5):
    # cosine distances of project A and XYZ <=> compute dot products of vector A with XYZ
    similarities = linear_kernel(vector_project, tfidf).flatten() # kernels = pairwise metrics in ML
    # extract the top 5 related documents
    projects = similarities.argsort()[:-(topx+1):-1]
    return [projects, similarities]
