from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from nltk.stem.porter import PorterStemmer
import nltk, string, sys, my_nlp
import json
import psycopg2
import psycopg2.extras
import sys
import os
import re
import time
import pandas as pd
import numpy as np
from my_threads import *

if len(sys.argv) != 2:
    print 'Usage: ', sys.argv[0], ' <db_password>'
    sys.exit(1)
db_password = sys.argv[1]

pd.options.mode.chained_assignment = None

# Projects
projectCols = [
    'id_project', 'id_source', 'title', 'fulldesc', 'url'
]

selProjectQuery = 'SELECT '
i = 0
for lbl in projectCols:
    if i != 0: selProjectQuery += ', '
    selProjectQuery += lbl
    i += 1
selProjectQuery += ' FROM project_summary INNER JOIN project USING(id_project, id_source)'

# ----- Initialize connection -----
connection = psycopg2.connect(
    host='localhost', port=5432,
    database='crowford', user='kael',
    password=db_password
)
cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

# ------ Select projects ------
cursor.execute(selProjectQuery)
projects = cursor.fetchall()

projects_data = []
for elt in projects:
    projects_data.append(elt['fulldesc'])

print '>PROJECTS<\n', 'Nb rows: ', len(projects_data)

data = my_nlp.get_clean_data(projects_data)
# extract count features & apply TF-IDF normalization & row-normalization
vectorizer = TfidfVectorizer(tokenizer=my_nlp.tokenize, stop_words='english', strip_accents='unicode')
tfidf = vectorizer.fit_transform(data)

start = time.clock()
errors = {}
projects_related = []
size_projects = len(projects_data)
for i in range(30001, size_projects-1):
    try:
        [projects_rel, similarities] = my_nlp.get_topx_related_projects(tfidf[i:i+1], tfidf, 5)

        for j in range(len(projects_rel)):
            if j == 0: continue
        
            project_rel_infos = projects[projects_rel[j]]
            project_rel = {
                'id_source': projects[i]['id_source'],
                'id_project': projects[i]['id_project'],
                'id_source_related': project_rel_infos['id_source'],
                'id_project_related': project_rel_infos['id_project'],
                'title_related': project_rel_infos['title'],
                'url_related': project_rel_infos['url'],
                'coef_similarity': float(similarities[projects_rel[j]])
            }
            projects_related.append(project_rel)
        
    except Exception as e:
        print "error!"
        errors[i] = e

    if (((i % 1000) == 0 and i > 0) or i == (size_projects - 1)):
        end = time.clock()
        print("Time: {0} | Elements: {1} | Total : {2} | Percentage : {3}").format(
            end - start, i, len(projects_data), i / len(projects_data) * 100
        )
        print("Nb of errors: {0}").format(len(errors))

        if (i % 5000) == 0 or i == (size_projects  - 1):
            dataset = pd.DataFrame(projects_related)
            dataset.to_csv('datasets/csv/projects_related_{0}.csv'.format(i),index=False,sep=',',encoding='utf-8')
            projects_related = []
            print("Dataset saved: projects_related_{0}.csv").format(i)
print 'Nb of errors: ', len(errors)


'''
dataset = pd.DataFrame(projects_related)


dataset.to_csv('datasets/csv/projects_related.csv',index=False,sep=',',encoding='utf-8')
print 'Nb rows: ', len(dataset.index)
'''
end = time.clock()
print("Execution time : {0}").format(end - start)
