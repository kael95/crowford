import csv
import json
import psycopg2
import os
import sys
import re
import time
import numpy as np

if len(sys.argv) != 2:
    print 'Usage: ', sys.argv[0], ' <db_password>'
    sys.exit(1)
db_password = sys.argv[1]

# ----- DATA LOADING & FUSION -----
authors = {}
with open('../datasets/csv/indiegogo_team_members.csv') as f:
    reader = csv.DictReader(f, delimiter=';')
    for row in reader:
        if (row['campaign_id'] not in authors):
            authors[row['campaign_id']] = []
        authors[row['campaign_id']].append(row)

perks = {}
with open('../datasets/csv/indiegogo_perks.csv') as f:
    reader = csv.DictReader(f, delimiter=';')
    for row in reader:
        if (row['campaign_id'] not in perks):
            perks[row['campaign_id']] = []
        perks[row['campaign_id']].append(row)

projects = []
with open('../datasets/csv/indiegogo_campaigns.csv') as f:
    reader = csv.DictReader(f, delimiter=';')
    for row in reader:
        if row['id'] not in authors: continue
        row['authors'] = authors[row['id']]
        if row['id'] in perks: row['packages'] = perks[row['id']]
        projects.append(row)

# ----- DATABASE INSERTS -----
# Useful functions
def strToInt(str):
    return int(str) if str else None

# Initialize connection
connection = psycopg2.connect(
    host='localhost', port=5432,
    database='crowford', user='kael',
    password=db_password
)
cursor = connection.cursor()

# Delete Data
delQuery = "DELETE FROM project WHERE id_source=2; DELETE FROM author WHERE id_source=2"
start = time.clock()
cursor.execute(delQuery)
connection.commit()
end = time.clock()
print("Execution time of delete queries: {0}").format(end - start)

# SQL Queries
insProjectQuery = '''
INSERT INTO project(
    id_project, id_source, category, title, url, date_end, summary, fulldesc,
    country, currency, money_percent, money_goal, money_pledged, nb_images,
    nb_characters, nb_supporters, nb_updates, nb_comments, finished, dt_creation
) VALUES {0} ON CONFLICT DO NOTHING'''
insProjectValues = []

insAuthorQuery = '''
INSERT INTO author(
    id_author, id_source, url_profile, username, nb_suc_campaigns_sup,
    nb_pen_campaigns_sup, nb_uns_campaigns_sup, nb_suc_campaigns_cre, 
    nb_pen_campaigns_cre, nb_uns_campaigns_cre, nb_campaigns_sup, nb_campaigns_cre
) VALUES {0} ON CONFLICT DO NOTHING'''
insAuthorValues = []

insAuthorProjectQuery = '''
INSERT INTO author_project(
    id_author, id_project, id_source
) VALUES {0} ON CONFLICT DO NOTHING'''
insAuthorProjectValues = []

insPerkQuery = '''
INSERT INTO perk(
    id_project, id_source, price, nb_sold, nb_max
) VALUES {0} ON CONFLICT DO NOTHING'''
insPerkValues = []

# Loop on projects
errors = {}
i = 1
start = time.clock()
len_data = len(projects)
nb_elts_skipped=0
for elt in projects:
    if 'money_goal' not in elt or 'money_collected' not in elt:
        i += 1
        continue

    if strToInt(elt['money_goal']) != 0:
        money_percent = round(strToInt(elt['money_collected']) / strToInt(elt['money_goal']) * 100)
    else:
        money_percent = 0

    try:
        # Insert Project Values
        insProjectValues.append((
            strToInt(elt['id']), 2, elt['category'], elt['title'], None, elt['date_end'],
            elt['summary'], elt['description'], elt['country'],elt['currency'],money_percent,
            strToInt(elt['money_goal']) if 'money_goal' in elt else None,
            strToInt(elt['money_collected']) if 'money_collected' in elt else None,
            None, strToInt(elt['nb_characters']), strToInt(elt['nb_supporters']), 
            None, None, elt['finished'] if 'finished' in elt else None, '2016-04-16'
        ))

        # Insert Author Values
        for author in elt['authors']:
            insAuthorValues.append((
                strToInt(author['id']), 2, None, author['name'], None,
                None, None,None,None,None, None, None
            ))
            insAuthorProjectValues.append((
                strToInt(author['id']), strToInt(elt['id']), 2
            ))

        # Insert Perk Values
        if 'packages' in elt:
            for perk in elt['packages']:
                if 'number_claimed' not in perk or 'number_available' not in perk: continue
                if strToInt(perk['number_available']) is None: continue
                if strToInt(perk['number_claimed']) is None: continue
                nbMax = strToInt(perk['number_claimed']) + strToInt(perk['number_available'])
                if (nbMax < 0 or nbMax > 10000): continue
                insPerkValues.append((
                    strToInt(elt['id']), 2, strToInt(perk['amount']), strToInt(perk['number_claimed']), nbMax
                ))
                
    except Exception as e:
        errors[elt['id']] = e

    # Avoid consuming a lot of memory by executing queries every 20 projects
    if ((i % 20) == 0 or i == len_data):
        # Project
        if insProjectValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insProjectValues)
            cursor.execute(insProjectQuery.format(str_values))

        # Author
        if insAuthorValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insAuthorValues)
            cursor.execute(insAuthorQuery.format(str_values))

        # AuthorProject
        if insAuthorProjectValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insAuthorProjectValues)
            cursor.execute(insAuthorProjectQuery.format(str_values))

        # Perk
        if insPerkValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insPerkValues)
            #print "\n<------>\n", str_values
            cursor.execute(insPerkQuery.format(str_values))

        # reset
        insProjectValues = []
        insAuthorValues = []
        insAuthorProjectValues = []
        insPerkValues = []

        end = time.clock()
        timedif = ((end - start) % 180)
        if ((timedif >=0 and timedif <= 20) or (timedif >= 160 and timedif < 180)):
            print("Time: {0} | Elements: {1} | Total: {2} | Percentage: {3}").format(
                end - start, i, len_data, i / len_data * 100
            )

    i += 1
connection.commit()
connection.close()
end = time.clock()
nb_errors=len(errors)
print("Execution time of processing + insert queries: {0}").format(end - start)
print("Number of errors: {0}").format(nb_errors)
print("Number of elements skipped: {0}").format(nb_elts_skipped)
print("Number of elements successfully inserted: {0}").format(len_data - nb_errors - nb_elts_skipped)

for k,v in errors.items():
    print("product id: ", k, " v: ", v)

