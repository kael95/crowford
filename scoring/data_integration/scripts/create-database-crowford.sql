-- Database : PostgreSQL

-- DROP TABLES
DROP TABLE IF EXISTS author_project;
DROP TABLE IF EXISTS author;
DROP TABLE IF EXISTS perk;
DROP TABLE IF EXISTS project_summary;
DROP TABLE IF EXISTS project_related;
DROP TABLE IF EXISTS project;

-- CREATE TABLE PROJECT
CREATE TABLE IF NOT EXISTS project(
	id_project int not null,
	id_source int not null,
	category varchar(100),
	title varchar(250),
	url varchar(250),
	date_end varchar(250),
	summary text,
	fulldesc text,
	country varchar(100),
	currency varchar(3),
	money_percent int,
	money_goal int,
	money_pledged int,
	nb_characters int,
	nb_images int,
	nb_supporters int,
	nb_updates int,
	nb_comments int,
	finished boolean,
	dt_creation date,

	primary key (id_project, id_source)
);

-- CREATE TABLE PROJECT SUMMARY
CREATE TABLE IF NOT EXISTS project_summary (
	id_project int not null,
	id_source int not null,
	nb_chr_title int,
	nb_chr_summary int,
	nb_chr_description int,
	nb_authors int,
	avg_campaigns_sup int,
	avg_campaigns_cre int,
	nb_perks int,
	avg_price int,
	avg_nb_available int,
	avg_nb_sold int,
	score_success boolean,
	is_finished boolean,
	date_creation varchar(250),
	location varchar(100),
	category varchar(100),
	
	primary key (id_project, id_source),
	foreign key (id_project, id_source) references project(id_project, id_source) on delete cascade
);

-- CREATE TABLE PROJECT RELATED
CREATE TABLE IF NOT EXISTS project_related (
       id_project int not null,
       id_source int not null,
       id_project_related int not null,
       id_source_related int not null,
       title_related varchar(250),
       url_related varchar(250),
       coef_similarity real,
       primary key (id_project, id_source, id_project_related, id_source_related),
       foreign key (id_project, id_source) references project(id_project, id_source) on delete cascade,
       foreign key (id_project_related, id_source_related) references project(id_project, id_source) on delete cascade
);

-- CREATE TABLE AUTHOR
CREATE TABLE IF NOT EXISTS author (
	id_author int not null,
	id_source int not null,
	url_profile varchar(250),
	username varchar(100),
	nb_suc_campaigns_sup int,
	nb_pen_campaigns_sup int,
	nb_uns_campaigns_sup int,
	nb_suc_campaigns_cre int,
	nb_pen_campaigns_cre int,
	nb_uns_campaigns_cre int,
	nb_campaigns_sup int,
	nb_campaigns_cre int,
	
	primary key (id_author, id_source)
);

-- CREATE TABLE AUTHOR-PROJECT
CREATE TABLE IF NOT EXISTS author_project (
	id_author int not null,
	id_project int not null,
	id_source int not null,

	primary key (id_author, id_project, id_source),
	foreign key (id_author, id_source) references author(id_author, id_source) on delete cascade,
	foreign key (id_project, id_source) references project(id_project, id_source) on delete cascade
);

-- CREATE TABLE PACKAGE
CREATE TABLE IF NOT EXISTS perk (
	id_perk serial not null,
	id_project int not null,
	id_source int not null,
	price int,
	nb_sold int,
	nb_max int,
	
	primary key (id_perk, id_project, id_source),
	foreign key (id_project, id_source) references project(id_project, id_source) on delete cascade
);

-- CREATE INDEXES
CREATE INDEX idx_pr_project_fk ON project_related(id_project, id_source);
CREATE INDEX idx_ps_project_fk ON project_summary(id_project, id_source);
CREATE INDEX idx_ap_author_fk ON author_project(id_author, id_source);
CREATE INDEX idx_ap_project_fk ON author_project(id_project, id_source);
CREATE INDEX idx_pe_project_fk ON perk(id_project, id_source);
