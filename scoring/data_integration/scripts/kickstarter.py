import json
import psycopg2
import sys
import os
import re
import time


if len(sys.argv) != 2:
    print 'Usage: ', sys.argv[0], ' <db_password>'
    sys.exit(1)
db_password = sys.argv[1]

# Useful functions
def strToInt(str):
    return int(str) if str else None

# Initialize connection
connection = psycopg2.connect(
    host='localhost', port=5432,
    database='crowford', user='kael',
    password=db_password
)
cursor = connection.cursor()

# Delete Data
delQuery = "DELETE FROM project WHERE id_source=1; DELETE FROM author WHERE id_source=1"
start = time.clock()
cursor.execute(delQuery)
connection.commit()
end = time.clock()
print("Execution time of delete queries: {0}").format(end - start)

# Load Data
data = json.loads(open('../datasets/json/kickstarter.json').read())

# SQL Queries
insProjectQuery = '''
INSERT INTO project(
    id_project, id_source, category, title, url, date_end, summary, fulldesc,
    country, currency, money_percent, money_goal, money_pledged, nb_images,
    nb_characters, nb_supporters, nb_updates, nb_comments, finished, dt_creation
) VALUES {0} ON CONFLICT DO NOTHING'''
insProjectValues = []

insAuthorQuery = '''
INSERT INTO author(
    id_author, id_source, url_profile, username, nb_suc_campaigns_sup,
    nb_pen_campaigns_sup, nb_uns_campaigns_sup, nb_suc_campaigns_cre, 
    nb_pen_campaigns_cre, nb_uns_campaigns_cre, nb_campaigns_sup, nb_campaigns_cre
) VALUES {0} ON CONFLICT DO NOTHING'''
insAuthorValues = []

insAuthorProjectQuery = '''
INSERT INTO author_project(
    id_author, id_project, id_source
) VALUES {0} ON CONFLICT DO NOTHING'''
insAuthorProjectValues = []

insPerkQuery = '''
INSERT INTO perk(
    id_project, id_source, price, nb_sold, nb_max
) VALUES {0} ON CONFLICT DO NOTHING'''
insPerkValues = []

# Loop on projects
errors = {}
i = 1
start = time.clock()
len_data = len(data)
nb_elts_skipped=0
for elt in data:
    if ('money_percent' in elt):
        money_percent = elt['money_percent'] if elt['money_percent'] else 100 if elt['success'] else 0
    else:
        money_percent = None

    try:
        # Insert Project Values
        insProjectValues.append((
            strToInt(elt['pid']), 1, elt['category'], elt['title'], elt['urlMainPage'], elt['date_end'],
            elt['summary'], elt['fulldesc'], elt['location'],elt['currency'],money_percent,
            elt['money_goal'] if 'money_goal' in elt else None, elt['money_pledged'] if 'money_pledged' in elt else None,
            elt['nbImages'], elt['nbCharacters'], elt['nbSupporters'], 
            None, None, elt['finished'] if 'finished' in elt else None, '2016-03-10'
        ))

        # Insert Author Values
        atLeastOneAuthor = False
        for author in elt['authors']:
            if (author['uid'].isdigit() is False): continue

            insAuthorValues.append((
                strToInt(author['uid']), 1, author['urlProfile'], author['name'], None,
                None, None,None,None,None, author['nbContribution'], author['nbCampaigns']
            ))
            insAuthorProjectValues.append((
                strToInt(author['uid']), strToInt(elt['pid']), 1
            ))
            atLeastOneAuthor = True

        if atLeastOneAuthor is False:
            del insProjectValues[-1]
            nb_elts_skipped += 1
            continue

        # Insert Perk Values
        for perk in elt['Packages']:
            insPerkValues.append((
                strToInt(elt['pid']), 1, perk['price'], perk['nbSold'],
                perk['nbMax'] if perk['nbMax'] and perk['nbMax'] != -1 else None
            ))

    except Exception as e:
        errors[elt['pid']] = e

    # Avoid consuming a lot of memory by executing queries every 20 projects
    if ((i % 20) == 0 or i == len_data):
        # Project
        if insProjectValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insProjectValues)
            cursor.execute(insProjectQuery.format(str_values))

        # Author
        if insAuthorValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insAuthorValues)
            cursor.execute(insAuthorQuery.format(str_values))

        # AuthorProject
        if insAuthorProjectValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insAuthorProjectValues)
            cursor.execute(insAuthorProjectQuery.format(str_values))

        # Perk
        if insPerkValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insPerkValues)
            cursor.execute(insPerkQuery.format(str_values))

        # reset
        insProjectValues = []
        insAuthorValues = []
        insAuthorProjectValues = []
        insPerkValues = []

        end = time.clock()
        timedif = ((end - start) % 180)
        if ((timedif >=0 and timedif <= 20) or (timedif >= 160 and timedif < 180)):
            print("Time: {0} | Elements: {1} | Total: {2} | Percentage: {3}").format(
                end - start, i, len_data, i / len_data * 100
            )

    i += 1
connection.commit()
connection.close()
end = time.clock()
nb_errors=len(errors)
print("Execution time of processing + insert queries: {0}").format(end - start)
print("Number of errors: {0}").format(nb_errors)
print("Number of elements skipped: {0}").format(nb_elts_skipped)
print("Number of elements successfully inserted: {0}").format(len_data - nb_errors - nb_elts_skipped)

'''
for k,v in errors.items():
    print("product id: ", k, " v: ", v)
'''
