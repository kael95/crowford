import json
import psycopg2
import psycopg2.extras
import sys
import os
import re
import time
import pandas as pd
import numpy as np

if len(sys.argv) != 2:
    print 'Usage: ', sys.argv[0], ' <db_password>'
    sys.exit(1)
db_password = sys.argv[1]

pd.options.mode.chained_assignment = None

# Projects
projectCols = [
    'id_project', 'id_source', 'category', 'title', 'summary',
    'fulldesc', 'country', 'money_goal', 'money_pledged', 'money_percent', 'nb_characters',
    'nb_images', 'nb_supporters', 'nb_updates', 'nb_comments', 'date_end', 'finished', 'dt_creation'
]

selProjectQuery = 'SELECT '
i = 0
for lbl in projectCols:
    if i != 0: selProjectQuery += ', '
    selProjectQuery += lbl
    i += 1
selProjectQuery += ' FROM project'

# Perks
perkCols = ['id_project', 'id_source', 'id_perk', 'price', 'nb_sold', 'nb_max']
selPerkQuery = 'SELECT '
i = 0
for lbl in perkCols:
    if i != 0: selPerkQuery += ', '
    selPerkQuery += lbl
    i += 1
selPerkQuery += ' FROM perk'

# Author
authorCols = ['id_project', 'id_source', 'id_author', 'nb_campaigns_sup', 'nb_campaigns_cre']
selAuthorQuery = 'SELECT '
i = 0
for lbl in authorCols:
    if i != 0: selAuthorQuery += ', '
    selAuthorQuery += lbl
    i += 1
selAuthorQuery += ' FROM author_project join author using(id_author,id_source)'

# ----- Initialize connection -----
connection = psycopg2.connect(
    host='localhost', port=5432,
    database='crowford', user='kael',
    password=db_password
)
start = time.clock()
cursor = connection.cursor()

# ------ Select projects ------
cursor.execute(selProjectQuery)
projects = cursor.fetchall()
dfProjects = pd.DataFrame(projects, columns=projectCols)

for i, r in dfProjects.iterrows():
    dfProjects.set_value(i, 'country', re.sub(r'(.+), (.+)', '\\2', r['country']))
    dfProjects.set_value(i, 'nb_chr_title',len(r['title']))
    dfProjects.set_value(i, 'nb_chr_summary',len(r['summary']))
    dfProjects.set_value(i, 'nb_chr_description',r['nb_characters'])
dfProjects.drop(['nb_characters','fulldesc', 'summary', 'title'],inplace=True,axis=1)

print '>PROJECTS<\n', 'Nb rows: ', len(dfProjects.index),'\nColumns: ', dfProjects.columns

# ----- Select authors -----
cursor.execute(selAuthorQuery)
authors = cursor.fetchall()
dfAuthors = pd.DataFrame(authors, columns=authorCols)

aggregations = {
    'id_author': {'nb_authors': 'count'},
    'nb_campaigns_sup': {'avg_campaigns_sup': 'mean'},
    'nb_campaigns_cre': {'avg_campaigns_cre': 'mean'}
}
dfAuthors = dfAuthors.groupby(['id_project', 'id_source']).agg(aggregations)
dfAuthors.columns = dfAuthors.columns.droplevel(0)
dfAuthors = dfAuthors.reset_index()

print '\n>AUTHORS<\n', 'Nb rows: ', len(dfAuthors.index),'\nColumns: ', dfAuthors.columns
print dfAuthors.head(10)

# ----- Select perks -----
cursor.execute(selPerkQuery)
perks = cursor.fetchall()
dfPerks = pd.DataFrame(perks, columns=perkCols)

def getNbAvailable(r):
    nb_available = None
    if not np.isnan(r['nb_max']) and not np.isnan(r['nb_sold']):
        nb_available = int(r['nb_max']) - int(r['nb_sold'])
        if nb_available < 0 or nb_available > int(r['nb_max']):
            nb_available = None
    return nb_available
dfPerks['nb_available'] = dfPerks.apply(getNbAvailable, axis=1)
dfPerks.drop(['nb_max'], inplace=True, axis=1)

aggregations = {
    'id_perk': {'nb_perks': 'count'},
    'price': {'avg_price': 'mean'},
    'nb_sold': {'avg_nb_sold': 'mean'},
    'nb_available': {'avg_nb_available': 'mean'}
}
dfPerks = dfPerks.groupby(['id_project', 'id_source']).agg(aggregations)
dfPerks.columns = dfPerks.columns.droplevel(0)
dfPerks = dfPerks.reset_index()

print '\n>PERKS<\n', 'Nb rows: ', len(dfPerks.index),'\nColumns: ', dfPerks.columns
print dfPerks.head(10)

# ----- MERGE -----
dfs = [dfProjects, dfAuthors, dfPerks]
dataset = reduce(lambda l,r: pd.merge(l,r,how='left',on=['id_project','id_source']),dfs)
dataset = dataset[dataset.nb_authors > 0]
def fix_projects_no_perks(row):
    if (np.isnan(row['nb_perks'])):
        return 0
    return row['nb_perks']
dataset['nb_perks'] = dataset.apply(fix_projects_no_perks, axis=1)

# ----- Response variable (success: 0, 1) -----
from datetime import datetime

reg_dt_end = {
    'pozible': r'(.+)\s+([0-9]{1,2})\s+(.+)\s+([0-9]{1,2}):([0-9]{1,2})\s+(.{2})(.+)',
    'others': r'([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})T([0-9]{1,2}):([0-9]{1,2}):(.+)'
}

def get_finished(row):
    if row['finished'] is not None: return row['finished']
    res = None
    if row['date_end']:
        dt = None
        if row['date_end'].startswith('Deadline'):
            tmp = re.sub(reg_dt_end['pozible'], '\\2 \\3 2016 \\4:\\5 \\6', row['date_end'])
            dt = datetime.strptime(tmp, '%d %b %Y %H:%M %p')
        else:
            tmp = re.sub(reg_dt_end['others'], '\\3 \\2 \\1 \\4:\\5', row['date_end'])
            dt = datetime.strptime(tmp, '%d %m %Y %H:%M')
        res = dt.date() <= row['dt_creation'] if dt is not None else None
    return res
    
def get_success(row):
    res = None
    if row['money_percent'] >= 100.0 or (row['money_pledged'] >= row['money_goal'] and row['money_goal'] > 0):
        res = 1
    elif row['finished'] and (row['money_percent'] < 100.0 or row['money_pledged'] < row['money_goal']):
        res = 0
    return res
    
dataset['finished'] = dataset.apply(get_finished, axis=1)
dataset['success'] = dataset.apply(get_success, axis=1)
dataset.drop(['date_end', 'money_percent', 'money_pledged'], inplace=True, axis=1)

print '\n>DATASET<\n', 'Nb rows: ', len(dataset.index),'\nColumns: ', dataset.columns
print dataset.head(10)
print dataset.isnull().any()
print 'Number of null values in column \'Finished\': ', dataset['finished'].isnull().sum()
print 'Number of valid values in column \'Finished\': ', dataset['finished'].count()
print 'Number of null values in column \'Success\': ', dataset['success'].isnull().sum()
print 'Number of valid values in column \'Success\': ', dataset['success'].count()

dataset.to_csv('datasets/csv/input_scoring_success.csv',index=False,sep=',',encoding='utf-8')
# ----- END OF EXECUTION -----
end = time.clock()
print "End time: ", end
print("Execution time : {0}").format(end - start)
