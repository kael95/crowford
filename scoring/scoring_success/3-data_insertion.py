import csv
import json
import psycopg2
import os
import sys
import re
import time
import numpy as np

if len(sys.argv) != 2:
    print 'Usage: ', sys.argv[0], ' <db_password>'
    sys.exit(1)
db_password = sys.argv[1]

# ----- DATA LOADING -----
projectsSummary = []
with open('datasets/csv/output_scoring_success.csv') as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        projectsSummary.append(row)

# ----- DATABASE INSERTS -----
# Useful functions
def strToInt(str):
    res = re.sub(r'([0-9]+).?([0-9]*)','\\1',str)
    return int(res) if res else None

# Initialize connection
connection = psycopg2.connect(
    host='localhost', port=5432,
    database='crowford', user='kael',
    password=db_password
)
cursor = connection.cursor()

# Delete Data
delQuery = "DELETE FROM project_summary"
start = time.clock()
cursor.execute(delQuery)
connection.commit()
end = time.clock()
print("Execution time of delete queries: {0}").format(end - start)

# SQL Queries
insProjectSummaryQuery = '''
INSERT INTO project_summary(
    id_project, id_source, nb_chr_title, nb_chr_summary, nb_chr_description,
    nb_authors, avg_campaigns_sup, avg_campaigns_cre, nb_perks, avg_price,
    avg_nb_available, avg_nb_sold, score_success, is_finished, date_creation,
    location, category
) VALUES {0} ON CONFLICT DO NOTHING'''
insProjectSummaryValues = []

# Loop on projects summary
errors = {}
i = 1
start = time.clock()
len_data = len(projectsSummary)
nb_elts_skipped=0
for elt in projectsSummary:
    try:
        # Insert Project Summary Values
        values = (
            strToInt(elt['id_project']), strToInt(elt['id_source']), strToInt(elt['nb_chr_title']), strToInt(elt['nb_chr_summary']),
            strToInt(elt['nb_chr_description']), strToInt(elt['nb_authors']), strToInt(elt['avg_campaigns_sup']), strToInt(elt['avg_campaigns_cre']),
            strToInt(elt['nb_perks']), strToInt(elt['avg_price']), strToInt(elt['avg_nb_available']), strToInt(elt['avg_nb_sold']),
            str(strToInt(elt['success'])), elt['finished']*1, elt['date_creation'], elt['country'], elt['category']
        )
        insProjectSummaryValues.append(values)
                
    except Exception as e:
        errors[elt['id_project']] = e

    # Avoid consuming a lot of memory by executing queries every 20 projects
    if ((i % 20) == 0 or i == len_data):
        # Project
        if insProjectSummaryValues:
            str_values = ', '.join(cursor.mogrify("%s", (x, )) for x in insProjectSummaryValues)
            cursor.execute(insProjectSummaryQuery.format(str_values))

        # reset
        insProjectSummaryValues = []

        end = time.clock()
        timedif = ((end - start) % 180)
        if ((timedif >=0 and timedif <= 20) or (timedif >= 160 and timedif < 180)):
            print("Time: {0} | Elements: {1} | Total: {2} | Percentage: {3}").format(
                end - start, i, len_data, i / len_data * 100
            )

    i += 1
connection.commit()
connection.close()
end = time.clock()
nb_errors=len(errors)
print("Execution time of processing + insert queries: {0}").format(end - start)
print("Number of errors: {0}").format(nb_errors)
print("Number of elements skipped: {0}").format(nb_elts_skipped)
print("Number of elements successfully inserted: {0}").format(len_data - nb_errors - nb_elts_skipped)

for k,v in errors.items():
    print("product id: ", k, " v: ", v)

