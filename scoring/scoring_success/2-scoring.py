import pandas as pd
import numpy as np
import random
from sklearn.metrics import roc_curve, auc, confusion_matrix, classification_report
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression

pd.options.mode.chained_assignment = None

# ----- USEFUL FUNCTIONS ------
def replaceMissingValByMean(df, numcols):
    df[numcols] = df[numcols].fillna(df[numcols].mean(), inplace=True)

# ----- DESCRIPTIVE ANALYTICS -----
dataset = pd.read_csv('datasets/csv/input_scoring_success.csv')

print '>> Columns: \n', dataset.columns
print '\n>> Head(10): \n', dataset.head(10)
print '\n>> Describe: \n', dataset.describe()

datasetColsType = {
    'id': ['id_source'],
    'cat': ['category', 'country'],
    'num': [
        'money_goal', 'nb_images', 'nb_supporters',
        'nb_updates', 'nb_comments', 'nb_chr_title',
        'nb_chr_summary', 'nb_chr_description', 'nb_authors',
        'avg_campaigns_sup', 'avg_campaigns_cre', 'nb_perks',
        'avg_price', 'avg_nb_available', 'avg_nb_sold'
    ],
    'response': 'success'
}

# ----- FLAG TRAINING & APPLICATION SETS -----
def getFlgTypeSet(r):
    if np.isnan(r['success']):
        return 'application'
    return 'training'
dataset['flg_type_set'] = dataset.apply(getFlgTypeSet, axis=1)
print 'Number of null values in column \'Success\': ', dataset['success'].isnull().sum()
print 'Number of valid values in column \'Success\': ', dataset['success'].count()

# ----- HANDLE CATEGORICAL DATA -----
dataset = dataset[(dataset['country'] != '?') & (dataset['country'])]
def adjustMoneyGoal(row):
    if (row['money_goal'] == 0):
        return None
    return row['money_goal']
dataset['money_goal'] = dataset.apply(adjustMoneyGoal, axis=1)
    
original_dataset = dataset.copy()
original_application = original_dataset[original_dataset['flg_type_set'] == 'application']
original_training = original_dataset[original_dataset['flg_type_set'] == 'training']

categorical_cols = datasetColsType['cat'] + ['success']
for var in categorical_cols:
    dataset[var] = LabelEncoder().fit_transform(dataset[var].astype('str'))
    
# ----- HANDLE MISSING VALUES -----
print 'Missing values before: \n', dataset.isnull().any()
'''
we do not inject variables such as nb_comments, avg_price, avg_nb_available
because we want to explain how a project is likely to be successful right after its creation
so this kind of information is not available at this stage
'''
explanatory_vars = datasetColsType['id'] + datasetColsType['cat'] + [
    'money_goal', 'nb_images', 'nb_chr_title', 'nb_chr_summary', 'nb_chr_description',
    'nb_authors', 'avg_campaigns_sup', 'avg_campaigns_cre', 'nb_perks', 'avg_price'
]
flg_miss_vals = []
for var in explanatory_vars:
    if dataset[var].isnull().any() == True:
        dataset['flg_na_' + var] = dataset[var].isnull() * 1
        flg_miss_vals.append('flg_na_' + var)
explanatory_vars += flg_miss_vals
replaceMissingValByMean(dataset, datasetColsType['num'])
print '\nMissing values after: \n', dataset.isnull().any()

# ----- SPLIT DATASET -----
print 'Explanatory_vars: \n', explanatory_vars

application = dataset[dataset['flg_type_set'] == 'application']
x_application = application[explanatory_vars].values

training_test = dataset[dataset['flg_type_set'] == 'training']
x_train, x_test, y_train, y_test = train_test_split(
    training_test[explanatory_vars].values,
    training_test[datasetColsType['response']].values,
    test_size=0.33, random_state=42
)

# ----- CLASSIFIERS & PERFORMANCES -----
random.seed(100)
classifiers = {
    'rf': RandomForestClassifier(n_estimators=100),
    'lr': LogisticRegression()
}
best = [0,0,'']
for classifier in classifiers:
    classifiers[classifier].fit(x_train, y_train)
    scores = classifiers[classifier].predict(x_test)
    fpr, tpr, thres = roc_curve(y_test, scores)
    print '\n---- PERFORMANCES ', classifier, ' ----\n'
    accuracy = auc(fpr, tpr)
    print 'fpr: ', fpr[1], ' tpr: ', tpr[1], ' accuracy: ', accuracy
    print '\nconfusion_matrix: \n', confusion_matrix(y_test, scores, labels=[1, 0])
    print '\nreport: \n', classification_report(y_test, scores, labels=[1, 0], target_names=['success', 'failure'])
    if accuracy > best[0]: best = [accuracy, scores, classifier]
print 'best classifier: ', best[2]


# Features importance
importances = classifiers[best[2]].feature_importances_
importances = 100.0 * (importances / importances.max())
mean_importances = np.mean(importances)
best_features = {}
i = 0
print '\nBest features:'
for var in explanatory_vars:
    if importances[i] >= mean_importances:
        best_features[var] = importances[i]
    i += 1

import operator
sorted_best_features = sorted(best_features.items(),key=operator.itemgetter(1),reverse=True)
for feature in sorted_best_features: print feature
print '\n'

# ----- PREDICTION ----- #
scores = classifiers[best[2]].predict(x_application)
original_application['success'] = scores
resultset = original_training.append(original_application)

from datetime import datetime
resultset['date_creation'] = datetime.now()
print 'number of columns: ', len(resultset.columns), ' | columns: \n', resultset.columns
resultset.to_csv('datasets/csv/output_scoring_success.csv', index=False)
