# -*- coding: utf-8 -*-

# Define here the models for your scraped items

# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class PackageItem(scrapy.Item):
    nbSold = scrapy.Field()
    nbMax = scrapy.Field()
    price = scrapy.Field()

class AuthorItem(scrapy.Item):
    sid = scrapy.Field()
    uid = scrapy.Field()
    urlProfile = scrapy.Field()

    name = scrapy.Field()
    nbCampaigns = scrapy.Field()
    nbContribution = scrapy.Field()
    pid = scrapy.Field()


class ProjectItem(scrapy.Item):
    sid = scrapy.Field() # source id
    pid = scrapy.Field() # project id
    urlMainPage = scrapy.Field()
    finished = scrapy.Field()
    success = scrapy.Field()

    title = scrapy.Field()
    category = scrapy.Field()
    summary = scrapy.Field()
    fulldesc = scrapy.Field()

    currency = scrapy.Field()

    money_goal = scrapy.Field()
    money_pledged = scrapy.Field()
    money_percent = scrapy.Field()

    date_end = scrapy.Field()

    location = scrapy.Field()

    nbImages = scrapy.Field()
    nbVideos = scrapy.Field()
    nbCharacters = scrapy.Field()
    nbSupporters = scrapy.Field()
    nbPackages = scrapy.Field()

    Packages = scrapy.Field()
    #Author = scrapy.Field()
