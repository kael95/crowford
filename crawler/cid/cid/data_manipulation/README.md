* Requirements:
	- Python 3.5
	- The following libraries : json

* Usage:
	- Command 1: python 1_JsonSplit.py
	- Command 2: python 2_JsonFilter.py
	- Command 3: python 3_JsonFusion.py

* Details:
These scripts extract the campaigns raw data from a JSON file "campaigns_details.json" to a filtered and reorganized JSON file "data_result.json"
The scripts shall be used by order as defined in the Usage section.

* Warning:
The ouput files are rewritten at every execution of the script.