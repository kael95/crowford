import json

file_campaigns = open("data_campaigns.json")
file_authors = open("data_authors.json")

file_result = open("data_result.json", "w", encoding="utf8")

file_result.write("[")

authors_data = json.load(file_authors)
print("number of authors: {}".format(len(authors_data)))
campaigns_data = json.load(file_campaigns)
print("number of campaigns: {}".format(len(campaigns_data)))

campaigns_index = dict()
for n in range(len(campaigns_data)):
    campaigns_data[n]["authors"] = list()
    pid = campaigns_data[n]["pid"]
    campaigns_index[pid] = n


for author in authors_data:
    try:
        pid = author["pid"]
        index = campaigns_index[pid]
        campaigns_data[index]["authors"].append(author)
    except Exception:
        print("Error while joining user {} to campaign {}".format(author["uid"], author["pid"]))

print("Fusion done.")

for n in range(len(campaigns_data)):
    if len(campaigns_data[n]) == 0:
        print("Campaign {} does not contain any author and shall be discarted.".format(campaigns_data[n]["pid"]))
        del campaigns_data[n]

for campaign in campaigns_data:
    file_result.write("{},\n".format(json.dumps(campaign)))

print("Export done.")

file_result.write("]")

file_result.close()
file_campaigns.close()
file_authors.close()