﻿import json
import random

filename_input = "data_campaigns_fusioned.json"
filename_output = "data_result_cleaned.json"

file_input = open(filename_input)
file_output = open(filename_output, "w", encoding="utf8")

campaigns_list = json.load(file_input)
file_input.close()

mandatory_attributes = ["money_percent", "money_pledged", "money_goal"]

cleaned_data = []
junk_data_with_goal = []
junk_data = []
for campaign_data in campaigns_list:
    missing_mandatory_attributes = False
    for attribute in mandatory_attributes:
        if attribute not in campaign_data:
            missing_mandatory_attributes = True
            break
    if missing_mandatory_attributes == False:
        cleaned_data.append(campaign_data)
    elif "money_goal" in campaign_data and campaign_data["money_goal"] > 0:
        junk_data_with_goal.append(campaign_data)
    else:
        junk_data.append(campaign_data)

print("Total campaigns:", len(campaigns_list))
print("Total campaigns with mandatory attributes:", len(cleaned_data))
print("Other campaigns with goal:", len(junk_data_with_goal))
print("Other campaigns without goal:", len(junk_data))

nb_junk_campaigns = len(junk_data)
nb_campaigns_to_save = 15000 - len(cleaned_data)

saved_campaigns = []
for i in range(nb_campaigns_to_save):

    random_index = random.randrange(0, len(junk_data))
    saved_campaigns.append(junk_data.pop(random_index))

print("Junk campaigns left:", len(junk_data))
print("Junk campaigns saved:", len(saved_campaigns))

cleaned_data += saved_campaigns
print("Final campaigns stored in file:", len(cleaned_data))
file_output.write(json.dumps(cleaned_data))
file_output.close()

print("Saving URLs of campaigns with missing goal...")
ffile = open("campaigns_with_missing_goals.csv", "w")

for campaign in junk_data:
    ffile.write(campaign["urlMainPage"]+"\n")
print("done")