﻿import json

file = open("kickstarter_campaigns.json")

file_campaigns = open("data_campaigns.json", "w", encoding="utf8")
file_authors = open("data_authors.json", "w", encoding="utf8")

file_campaigns.write("[\n")
file_authors.write("[\n")

data = json.load(file)

nb_objects = len(data)
print("number of objects : {}".format(nb_objects))

for line in data:
    try:
        # object is a campaign
        line["fulldesc"] = line["fulldesc"][:1000]
        file_campaigns.write("{},\n".format(json.dumps(line)))
    except Exception:
        # object is a user
        file_authors.write("{},\n".format(json.dumps(line)))


file.close()

file_campaigns.write("]")
file_authors.write("]")

file_campaigns.close()
file_authors.close()
