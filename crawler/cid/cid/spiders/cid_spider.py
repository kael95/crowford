#
#
#

import scrapy
import urlparse
import time
import re

from cid.items import ProjectItem
from cid.items import PackageItem
from cid.items import AuthorItem
from urlparse import urljoin


class CidSpider(scrapy.Spider):
    name = "cid"
    allowed_domains = ["kickstarter.com"]

    def parse_author(self, response):
        name = response.selector.xpath('//div[@id="profile_bio"]/h2/text()')[0].extract()
        counts = response.selector.xpath('//span[@class="count"]/text()')
        item = AuthorItem()

        item['sid'] = 0
        item['uid'] = response.meta['uid']
        item['urlProfile'] = response.url

        item['name'] = name.encode('utf-8').strip(' \n\t\r')
        item['nbCampaigns'] = int(counts[1].extract().encode('utf-8'))
        item['nbContribution'] = int(counts[0].extract().encode('utf-8'))

        item['pid'] = response.meta['pid']

        yield item

    def parse_creator_bio(self, response):
        url = response.selector.xpath('//a[@class="green-dark"]/@href').extract()[0]
        url = url.encode('utf-8')
        uid = re.sub('/profile/', '', url.encode('utf-8'))
        url = response.urljoin(url)
        
        yield self.generate_author(url, uid, response.meta['pid'].encode('utf-8'))

    def parse_project(self, response):
        item = ProjectItem() 
        
        title = response.selector.xpath('//h2[@class="normal mb1"]/a[@class="green-dark"]/text()')[0].extract()
        category = response.selector.xpath('//div[@class="NS_projects__category_location"]/a[@class="grey-dark mr3 nowrap"]')[1].xpath('text()').extract()
        summary = response.selector.xpath('//p[@class="h3 mb3"]/text()').extract()

        currency = response.selector.xpath('//div[@class="num h1 bold nowrap"]/data/@data-currency').extract()
        money_pledged = response.selector.xpath('//div[@class="num h1 bold nowrap"]/data/@data-value').extract()
        money_goal = response.selector.xpath('//div[@class="num h1 bold nowrap"]/@data-goal').extract()

        date_end = response.selector.xpath('//div[@class="ksr_page_timer poll stat"]/@data-end_time').extract()
        if date_end == []:
            date_end = response.selector.xpath('//div[@class="mb0 h5"]/data/@data-value').extract()

        location = response.selector.xpath('//h2[@class="normal mb1"]/a[@class="green-dark"]/text()')[0].extract()
        location = response.selector.xpath('//div[@class="NS_projects__category_location"]/a[@class="grey-dark mr3 nowrap"]')[0].xpath('text()').extract()
        nbBackers = response.selector.xpath('//div[@class="num h1 bold"]/@data-backers-count').extract()

        imgs_in_desc = response.selector.xpath('//div[@class="full-description js-full-description responsive-media formatted-lists"]//img/@src').extract()
        videos = response.selector.xpath('//video')
        full_desc = response.selector.xpath('//div[@class="full-description js-full-description responsive-media formatted-lists"]//text()').extract()

        all_packages = response.selector.xpath('//div[@class="pledge__info"]')
		
        item['sid'] = 0
        item['pid'] = response.meta['pid'].encode('utf-8')
        item['urlMainPage'] = response.url
        item['finished'] = False
        item['success'] = False

        item['title'] = title.encode('utf-8').strip(' \t\n\r')
        item['category'] = category[0].encode('utf-8').strip(' \t\n\r')
        item['summary'] = summary[0].encode('utf-8').strip(' \t\n\r')
        item['fulldesc'] = ''.join(full_desc).encode('utf-8').strip(' \r\t\n')

        item['currency'] = currency[0].encode('utf-8').strip(' \t\n\r')

        item['money_goal'] = float(money_goal[0].encode('utf-8'))
        item['money_pledged'] = float(money_pledged[0].encode('utf-8'))
        item['money_percent'] = item['money_pledged'] / item['money_goal'] * 100
        
        item['date_end'] = date_end[0].encode('utf-8')

        item['location'] = location[0].encode('utf-8').strip(' \t\n\r') 

        item['nbImages'] = len(imgs_in_desc)
        item['nbVideos'] = len(videos)
        item['nbCharacters'] = len(item['fulldesc'])
        item['nbSupporters'] = int(re.sub('[^0-9.]', '', nbBackers[0].encode('utf-8')))

        item['nbPackages'] = len(all_packages)

        packages = []
        for pledge in all_packages:
            pledge_amount = pledge.xpath('h2/text()').extract()
            pledge_backer = pledge.xpath('p[@class="pledge__backer-count"]/text()').extract()
            limited = pledge.xpath('p[@class="pledge__backer-count"]/span[@class="pledge__limit"]/text()').extract()
            limited_gone = pledge.xpath('p[@class="pledge__backer-count"]/span[@class="pledge__limit reward__limit--all-gone"]/text()').extract()
            pack = PackageItem()
            packages += []
            pack['nbSold'] = ''.join(pledge_backer).encode('utf-8').strip(' \n\t\r')
            pack['nbSold'] = int(re.sub('[^0-9.]', '', re.sub('\ backers?$', '', pack['nbSold'])))
            pack['price'] = ''.join(pledge_amount).encode('utf-8').strip(' \n\t\r')
            pack['price'] = int(re.sub('[^0-9.]', '', pack['price']))
            if limited == []:
                pack['nbMax'] = -1 if limited_gone == [] else pack['nbSold']
            else:
                pack['nbMax'] = re.sub('[^0-9 .]', '', limited[0].encode('utf-8')).split()[1]
                pack['nbMax'] = int(pack['nbMax'])
            packages.append(pack)
        item['Packages'] = packages
            
        yield item

    def parse_success_project(self, response):
        item = ProjectItem()
        
        title = response.selector.xpath('//a[@class="hero__link"]/text()')[0].extract()

        if (len(response.selector.xpath('//div[@class="NS_projects__category_location"]/a[@class="grey-dark mr3 nowrap"]')) < 2):
          category = response.selector.xpath('//div[@class="NS_projects__category_location"]/a[@class="grey-dark mr3 nowrap"]')[0].xpath('text()').extract()
          location = [""]
        else :
          category = response.selector.xpath('//div[@class="NS_projects__category_location"]/a[@class="grey-dark mr3 nowrap"]')[1].xpath('text()').extract()
          location = response.selector.xpath('//div[@class="NS_projects__category_location"]/a[@class="grey-dark mr3 nowrap"]')[0].xpath('text()').extract()
        summary = response.selector.xpath('//div[@class="NS_project_profiles__blurb"]//text()').extract()

        currency = response.selector.xpath('//div[@class="num h1 bold nowrap"]/data/@data-currency').extract()
        money = response.selector.xpath('//div[@class="NS_projects__spotlight_stats"]/span/text()').extract()

        date_end = ""

        nbBackers = response.selector.xpath('//div[@class="NS_projects__spotlight_stats"]/b/text()').extract()

        imgs_in_desc = response.selector.xpath('//div[@class="full-description js-full-description responsive-media formatted-lists"]//img/@src').extract()
        videos = response.selector.xpath('//video')
        full_desc = response.selector.xpath('//div[@class="full-description js-full-description responsive-media formatted-lists"]//text()').extract()

        all_packages = response.selector.xpath('//div[@class="pledge__info"]')
		
        item['sid'] = 0
        item['pid'] = response.meta['pid'].encode('utf-8')
        item['urlMainPage'] = response.url
        item['finished'] = True
        item['success'] = True

        item['title'] = title.encode('utf-8').strip(' \t\n\r')
        item['category'] = category[0].encode('utf-8').strip(' \t\n\r') 
        item['summary'] = summary[0].encode('utf-8').strip(' \t\n\r')
        item['fulldesc'] = ''.join(full_desc).encode('utf-8').strip(' \r\t\n')

        item['currency'] = ""

        item['money_goal'] = 0 
        
        item['date_end'] = ""

        item['location'] = location[0].encode('utf-8').strip(' \t\n\r')

        item['nbImages'] = len(imgs_in_desc)
        item['nbVideos'] = len(videos)
        item['nbCharacters'] = len(item['fulldesc'])
        item['nbSupporters'] = int(re.sub('[^0-9.]', '', nbBackers[0].encode('utf-8')))

        item['nbPackages'] = len(all_packages)

        packages = []
        for pledge in all_packages:
            pledge_amount = pledge.xpath('h2/text()').extract()
            pledge_backer = pledge.xpath('p[@class="pledge__backer-count"]/text()').extract()
            limited = pledge.xpath('p[@class="pledge__backer-count"]/span[@class="pledge__limit"]/text()').extract()
            limited_gone = pledge.xpath('p[@class="pledge__backer-count"]/span[@class="pledge__limit reward__limit--all-gone"]/text()').extract()
            pack = PackageItem()
            packages += []
            pack['nbSold'] = ''.join(pledge_backer).encode('utf-8').strip(' \n\t\r')
            pack['nbSold'] = int(re.sub('[^0-9.]', '', re.sub('\ backers?$', '', pack['nbSold'])))
            pack['price'] = ''.join(pledge_amount).encode('utf-8').strip(' \n\t\r')
            pack['price'] = int(re.sub('[^0-9.]', '', pack['price']))
            if limited == []:
                pack['nbMax'] = -1 if limited_gone == [] else pack['nbSold']
            else:
                pack['nbMax'] = re.sub('[^0-9 .]', '', limited[0].encode('utf-8')).split()[1]
                pack['nbMax'] = int(pack['nbMax'])
            packages.append(pack)
        item['Packages'] = packages
            
        yield item

    def parse_discover(self, response):
        for selCard in response.selector.xpath('//div[@class="js-score-tracking project-card project-card-tall project-card-wrap"]'):
            pid = selCard.xpath('@data-pid').extract()
            selCard = selCard.xpath('div[@class="project-card-content"]')
            for selTitle in selCard.xpath('h6[@class="project-title"]'):
                url = selTitle.xpath('a/@href').extract()[0]
            url2 = re.sub('\?ref=category_newest', '/creator_bio', url.encode('utf-8'))
            url = response.urljoin(url)
            url2 = response.urljoin(url2)
            yield self.generate_creator_bio(url2, pid)
            yield self.generate_project(url, pid)

        if response.selector.xpath('//div[@class="js-score-tracking project-card project-card-tall project-card-wrap"]') != [] and response.meta['index'] < 200:
            yield self.generate_discover_page(response.meta['category'], response.meta['index'] + 1)

    def parse_discover_successfull(self, response):
        for selCard in response.selector.xpath('//div[@class="project-card project-card--spotlight project-card-tall"]'):
            pid = selCard.xpath('@data-pid').extract()
            selCard = selCard.xpath('div[@class="project-card-content"]')
            for selTitle in selCard.xpath('div[@class="project-profile-title"]'):
                url = selTitle.xpath('a/@href').extract()[0]
            url = re.sub('\?ref=category_newest', '/description', url.encode('utf-8'))
            url2 = re.sub('/description', '/creator_bio', url)
            url = response.urljoin(url)
            url2 = response.urljoin(url2)
            yield self.generate_creator_bio(url2, pid)
            yield self.generate_success_project(url, pid)

        if response.selector.xpath('//div[@class="project-card project-card--spotlight project-card-tall"]') != [] and response.meta['index'] < 200:
            yield self.generate_discover_successfull_page(response.meta['category'], response.meta['index'] + 1)

    def generate_author(self, url, uid, pid):
        req = scrapy.http.Request(
            url = url,
            callback = self.parse_author,
            meta = {'uid' : uid, 'pid' : pid}
        )
        return req

    def generate_creator_bio(self, url, pid):
        req = scrapy.http.Request(
            url = url,
            callback = self.parse_creator_bio,
            meta = {'pid' : pid[0]}
        )
        return req

    def generate_project(self, url, pid):
        req = scrapy.http.Request(
            url = url,
            callback = self.parse_project,
            meta = {'pid' : pid[0]}
        )
        return req

    def generate_success_project(self, url, pid):
        req = scrapy.http.Request(
            url = url,
            callback = self.parse_success_project,
            meta = {'pid' : pid[0]}
        )
        return req

    def generate_discover_page(self, cat, index):
        req = scrapy.http.Request(
            url = "https://www.kickstarter.com/discover/advanced?category_id={0}&sort=newest&page={1}".format(cat, index),
            callback = self.parse_discover,
            meta = {'category' : cat, 'index' : index}
        )
        return req

    def generate_discover_successfull_page(self, cat, index):
        req = scrapy.http.Request(
            url = "https://www.kickstarter.com/discover/advanced?state=successful&category_id={0}&sort=newest&page={1}".format(cat, index),
            callback = self.parse_discover_successfull,
            meta = {'category' : cat, 'index' : index}
        )
        return req
        
    def start_requests(self):
        urls = []
        categories = [3, 1, 6, 7, 241, 239, 362]
        for i in range(0, 7):
            urls.append(self.generate_discover_page(categories[i], 0))
            urls.append(self.generate_discover_successfull_page(categories[i], 0))
        for i in range(9, 19):
            urls.append(self.generate_discover_page(i, 0))
            urls.append(self.generate_discover_successfull_page(i, 0))
        for i in range(20, 55):
            urls.append(self.generate_discover_page(i, 0))
            urls.append(self.generate_discover_successfull_page(i, 0))
        for i in range(270, 275):
            urls.append(self.generate_discover_page(i, 0))
            urls.append(self.generate_discover_successfull_page(i, 0))
        for i in range(281, 343):
            urls.append(self.generate_discover_page(i, 0))
            urls.append(self.generate_discover_successfull_page(i, 0))
        return urls

            


