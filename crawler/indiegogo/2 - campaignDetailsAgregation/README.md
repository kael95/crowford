* Requirements:
	- Python 3.5
	- The following libraries : pickle, re, httplib2, json, csv

* Usage:
	- Input: "campaigns_id.csv" file to the root of the script
	- Command: python ProjectDetailsAgregator.py
	- Exit: Ctrl+C then "y" or Ctrl+C again to end the script routine.
	- Output: all the campaigns details are saved in 4 different CSV files:
		* indiegogo_campaigns_details.csv
		* indiegogo_team_members_details.csv
		* indiegogo_perks_details.csv
		* indiegogo_secret_perks_details.csv

* Details:
This script extracts the campaigns ID from a CSV file and gather their details from the Indiegogo API. Failed or aborted (e.g.: after force exit) campaign requests will be stored in in a CSV file "campaigns_missed.csv" and successful requests in "campaigns_done.csv". This allows to reuse the script with the missed campaigns from a previous script execution.

* Warning:
	- Indiegogo API is protected against request overload. This may result in an unability for the script to gather more campaigns details using the same IP address.
	- The ouput files are rewritten at every execution of the script.