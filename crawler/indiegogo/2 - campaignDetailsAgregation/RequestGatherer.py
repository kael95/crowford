﻿import httplib2
import json

class RequestGatherer:

    def __init__(self, extractors):
        #self.http = httplib2.Http('.cache')
        #self.http.add_credentials('user', 'pass')
        self.extractors = extractors

    def request(self, url):
        #update : new connection per request to avoid bot detection
        self.http = httplib2.Http('.cache')
        self.http.add_credentials('user', 'pass')
        r, content = self.http.request(url, 'GET')
        data = json.loads(content.decode('utf-8'))
        extracted_attributes = list()
        for extractor in self.extractors:
            extracted_attributes.append(extractor().extractAttributes(data))
        return extracted_attributes

if __name__=="__main__":
    from Extractors import ProjectDetails
    rg = RequestGatherer([ProjectDetails])
    rg.request("https://www.indiegogo.com/private_api/campaigns/1640793")