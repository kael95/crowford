﻿import re

class Extractor(object):

    def __init__(self):
        self.compiler = re.compile(r'<.*?>')

    def getFilename(self):
        return ""

    def extractAttributes(self, json_dict):
        attributes = dict()
        return attributes

class ProjectsList(Extractor):

    def extractAttributes(self, json_dict):
        campaign_list = list()
        for campaign in json_dict['campaigns']:
            campaign_attributes = dict()
            campaign_attributes['id'] = campaign['id']
            campaign_attributes['url'] = campaign['url']
            campaign_attributes['collected_percentage'] = campaign['collected_percentage']
            campaign_list.append(campaign_attributes)
        return campaign_list

class ProjectDetails(Extractor):

    def extractAttributes(self, json_dict):
        attributes = dict()
        attributes['id'] = json_dict['response']['id']
        attributes['category'] = json_dict['response']['category']['name']
        attributes['title'] = json_dict['response']['title']
        attributes['money_goal'] = json_dict['response']['goal']
        attributes['money_collected'] = json_dict['response']['collected_funds']
        attributes['nb_supporters'] = json_dict['response']['contributions_count']
        attributes['currency'] = json_dict['response']['currency']['iso_code']
        attributes['country'] = json_dict['response']['country']
        attributes['city'] = json_dict['response']['city']
        attributes['funding_days'] = json_dict['response']['funding_days']
        attributes['date_start'] = json_dict['response']['funding_started_at']
        attributes['date_end'] = json_dict['response']['funding_ends_at']
        attributes['summary'] = json_dict['response']['tagline']
        attributes['description'] = self.compiler.sub('', json_dict['response']['description_text'])[:1000]
        attributes['id_creator'] = json_dict['response']['team_members'][0]['id']
        attributes['nb_team_members'] = len(json_dict['response']['team_members'])
        attributes['nb_perks'] = len(json_dict['response']['perks'])
        attributes['nb_secret_perks'] = len(json_dict['response']['secret_perks'])
        attributes['nb_characters'] = len(json_dict['response']['description_text'])
        return [attributes]

class TeamMembersDetails(Extractor):

    def extractAttributes(self, json_dict):
        team_member_list = list()
        campaign_id = json_dict['response']['id']
        for team_member in json_dict['response']['team_members']:
            team_member_attributes = dict()
            team_member_attributes['campaign_id'] = campaign_id
            team_member_attributes['id'] = team_member['id']
            team_member_attributes['account_id'] = team_member['account_id']
            team_member_attributes['name'] = team_member['name']
            team_member_attributes['is_owner'] = team_member['owner']
            team_member_attributes['status'] = team_member['status']
            team_member_attributes['role'] = team_member['role']
            team_member_list.append(team_member_attributes)
        return team_member_list

class PerksDetails(Extractor):

    def extractAttributes(self, json_dict):
        perks_list = list()
        campaign_id = json_dict['response']['id']
        for perk in json_dict['response']['perks']:
            perks_attributes = dict()
            perks_attributes['campaign_id'] = campaign_id
            perks_attributes['id'] = perk['id']
            perks_attributes['label'] = perk['label']
            perks_attributes['amount'] = perk['amount']
            perks_attributes['number_claimed'] = perk['number_claimed']
            perks_attributes['number_available'] = perk['number_available']
            perks_list.append(perks_attributes)
        return perks_list

class SecretPerksDetails(Extractor):

    def extractAttributes(self, json_dict):
        secret_perks_list = list()
        campaign_id = json_dict['response']['id']
        for secret_perk in json_dict['response']['secret_perks']:
            secret_perks_attributes = dict()
            secret_perks_attributes['campaign_id'] = campaign_id
            secret_perks_attributes['id'] = secret_perk['id']
            secret_perks_attributes['label'] = secret_perk['label']
            secret_perks_attributes['amount'] = secret_perk['amount']
            secret_perks_attributes['number_claimed'] = secret_perk['number_claimed']
            secret_perks_attributes['number_available'] = secret_perk['number_available']
            secret_perks_list.append(secret_perks_attributes)
        return secret_perks_list
