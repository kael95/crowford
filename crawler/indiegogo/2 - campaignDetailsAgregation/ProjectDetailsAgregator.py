﻿from threading import Thread, RLock
from Extractors import ProjectDetails, TeamMembersDetails, PerksDetails, SecretPerksDetails
from RequestGatherer import RequestGatherer
from csv import DictWriter, DictReader
from time import time, sleep
import signal
import sys

class ProjectDetailsAgregator(Thread):
    """description of class"""
    
    data_attributes_queue = None
    missed_request = None
    filenames = ["indiegogo_campaigns_details.csv",
                 "indiegogo_team_members_details.csv",
                 "indiegogo_perks_details.csv",
                 "indiegogo_secret_perks_details.csv"]

    attributes = [['id','category','title','money_goal','money_collected',
                                      'nb_supporters','currency','country','city','funding_days',
                                      'date_start','date_end','summary','description','id_creator',
                                      'nb_team_members','nb_perks','nb_secret_perks','nb_characters'],
                  ['campaign_id','id','account_id','name','is_owner','status','role'],
                  ['campaign_id','id','label','amount','number_claimed','number_available'],
                  ['campaign_id','id','label','amount','number_claimed','number_available']]
    def __init__(self, request_ids):
        Thread.__init__(self)
        self.lock = RLock()
        self.data_attributes_queue = []
        self.campaigns_done_queue = []
        self.missed_request = []
        self.request_ids = request_ids

    def add_attributes_data_in_queue(self, data):
        with self.lock:
            self.data_attributes_queue.append(data)

    def get_campaigns_done(self):
        with self.lock:
            data = list(self.campaigns_done_queue)
            self.campaigns_done_queue = []
        return data

    def add_campaigns_done_in_queue(self, data):
        with self.lock:
            self.campaigns_done_queue.append(data)

    def get_attributes_data_in_queue(self):
        with self.lock:
            data = list(self.data_attributes_queue)
            self.data_attributes_queue = []
        return data

    def add_missed_request(self, missed_request):
        with self.lock:
            self.missed_request.append(missed_request)

    def get_missed_requests(self):
        with self.lock:
            data = list(self.missed_request)
        return data

    def open_files(self):
        self.files = []
        self.writers = []
        for index in range(len(self.filenames)):
            file = open(self.filenames[index], 'w', encoding="utf8")
            writer = DictWriter(file, self.attributes[index], delimiter=';', lineterminator='\n', dialect='excel')
            writer.writeheader()
            self.files.append(file)
            self.writers.append(writer)

    def close_files(self):
        for file in self.files:
            file.close()

    def write_data(self, data):
        if len(self.writers) != len(data):
            print("Missing data to write")
            return False
        for index in range(len(self.writers)):
            self.writers[index].writerows(data[index])
        return True

    def run(self):
        extractors = [ProjectDetails, TeamMembersDetails, PerksDetails, SecretPerksDetails]
        request_gatherer = RequestGatherer(extractors)
        request_prefix = "https://www.indiegogo.com/private_api/campaigns/"
        for campaign_id in self.request_ids:
            new_request = request_prefix + str(campaign_id)
            retry = True
            while (retry):
                try:
                    list_attributes = request_gatherer.request(new_request)
                    self.add_attributes_data_in_queue(list_attributes)
                    self.add_campaigns_done_in_queue(campaign_id)
                    retry = False
                except Exception:
                    self.add_missed_request(campaign_id)
                    sleep(1)
            sleep(1)
        print("no more request, thread ends", len(self.missed_request))
        sleep(5)
        return

def handle(signum, frame):
    signal.signal(signal.SIGINT, original_sigint)
    try:
        if input("\n Quit data agregation? (y/n)> ").lower().startswith('y'):
            repeated = repeat_limit
    except KeyboardInterrupt:
        print("Force quitting.")
        repeated = repeat_limit    
    signal.signal(signal.SIGINT, handle)

if __name__=="__main__":

    nb_threads = 8
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, handle)

    primery_agregator = ProjectDetailsAgregator([])
    file = open("campaigns_id.csv", encoding='utf8')
    fields = ["id", "url", "collected_percentage"]
    reader = DictReader(file, fields, delimiter=';', lineterminator='\n', dialect='excel')

    imported_campaign_id = []
    for row in reader:
        imported_campaign_id.append(row['id'])
    file.close()
    pending_campaigns = list(imported_campaign_id)

    requests_per_thread = int(len(imported_campaign_id) / nb_threads + 1)
    print("requests to make:", len(imported_campaign_id))
    print("available threads:", nb_threads)
    print("requests per thread:", requests_per_thread)
    total_requests = len(imported_campaign_id)

    campaign_id_for_each_thread = []
    while len(imported_campaign_id) > 0:
        campaign_id_for_each_thread.append(imported_campaign_id[:requests_per_thread])
        imported_campaign_id = imported_campaign_id[requests_per_thread:]

    threads = []
    for n in range(len(campaign_id_for_each_thread)):
        new_thread = ProjectDetailsAgregator(campaign_id_for_each_thread[n])
        threads.append(new_thread)
        print("Thread #{} will execute {} requests".format(n, len(campaign_id_for_each_thread[n])))
        new_thread.start()

    campaign_data_list = []
    primery_agregator.open_files()
    time_start = time()
    threads_still_alive = True
    count_requests_done = 0
    eta = 0
    repeat_limit = 200
    repeated = 0

    while ((threads_still_alive or len(campaign_data_list) > 0)) and (repeated <= repeat_limit):
        threads_still_alive = False

        for thread in threads:
            campaign_data_list += thread.get_attributes_data_in_queue()
            if thread.is_alive():
                threads_still_alive = True

        count_requests_done += len(campaign_data_list)
        if len(campaign_data_list) == 0:
            repeated += 1
        else:
            repeated = 0
        for attributes_list in campaign_data_list:
            primery_agregator.write_data(attributes_list)
        campaign_data_list = []

        elapsed_time = time() - time_start
        if count_requests_done > 0:
            eta = total_requests * elapsed_time / count_requests_done - elapsed_time

        print("{}% done ({} / {}) - elapsed time: {} seconds - {} RPS - ETA: {} seconds".format(round(count_requests_done/total_requests*100),
                                                                                              count_requests_done,
                                                                                              total_requests,
                                                                                              round(elapsed_time,1),
                                                                                              round(count_requests_done/(elapsed_time + 0.0001),3),
                                                                                              round(eta)))
        
        if count_requests_done == total_requests:
            threads_still_alive = False
        sleep(2)
    primery_agregator.close_files()
    print("finished, recovering potential missed requests")

    campaigns_done = open("campaigns_done.csv", "w")
    campaigns_missed = open("campaigns_missed.csv", "w")

    list_of_campaigns_done = []
    list_of_campaigns_missed = []

    for thread in threads:
        list_of_campaigns_done += thread.get_campaigns_done()

    for each_campaign in pending_campaigns:
        if each_campaign not in list_of_campaigns_done:
            list_of_campaigns_missed.append(each_campaign)

    print ("Done campaigns:", len(list_of_campaigns_done))
    print ("Missed campaigns:", len(list_of_campaigns_missed))

    for done in list_of_campaigns_done:
        string = str(done) + "\n"
        campaigns_done.write(string)

    for missed in list_of_campaigns_missed:
        string = str(missed) + "\n"
        campaigns_missed.write(string)

        
    campaigns_done.close()
    campaigns_missed.close()