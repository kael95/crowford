﻿from Extractors import ProjectsList
from RequestGatherer import RequestGatherer
from ProgressRecorder import ProgressRecorder
from csv import DictWriter
from time import time

class CampaignsRequest:
    
    filename = "campaigns_id_list.csv"
    record_filename = "campaigns_id_list(records).csv"
    prefix_request = "https://www.indiegogo.com/private_api/explore?filter_funding=&filter_percent_funded=&filter_quick=all&filter_status=&per_page=100&pg_num="
    seconds_between_status = 10
    seconds_until_abortion = 120

    def run(self):
        request_gatherer = RequestGatherer([ProjectsList])
        progress_recorder = ProgressRecorder()
        try:
            file = open(self.filename, 'w')
        except Exception:
            print("Can't open file, make sure it is not open or protected.")
            return
        writer = DictWriter(file, ["id", "collected_percentage", "url"], delimiter=';', lineterminator='\n', dialect='excel')
        writer.writeheader()

        continue_requesting = True
        current_page = 1

        time_started = time()
        time_last_status = time_started - self.seconds_between_status
        time_last_success = time_started

        # tant que des campagnes sont encore agregees, on continue a faire des requetes
        while (continue_requesting is True):
            next_request = self.prefix_request + str(current_page)
            # exception si la requete echoue
            # ou si les attribues recherches sont incorrects (ex: due a une mauvaise requete)
            #try:
            list_attributes = request_gatherer.request(next_request)
            if (len(list_attributes[0]) > 0):
                writer.writerows(list_attributes[0])
                progress_recorder.addRequestSuccess(next_request)
                time_last_success = time()
            else:
                # il n'y avait pas de campagnes lors de la precedente requete, il n'y a donc plus de page de campagne a agreger
                continue_requesting = False
            #except Exception:
            #    progress_recorder.addRequestFailure(next_request)
            #    print(Exception)
            #    print("Error with projects page number " + str(current_page))
            current_page += 1


            current_time = time()
            # affichage d'informations concernant l'etat de l'agregation + enregistrement du status
            if (current_time - time_last_status >= self.seconds_between_status):
                elapsed_time = round(time() - time_started, 2)
                success_requests = len(progress_recorder.request_success)
                failure_requests = len(progress_recorder.request_failure)
                str_elapsed_time = "Elapsed time: " + str(elapsed_time)
                str_success_requests = "Success: " + str(success_requests)
                str_failure_requests = "Failure: " + str(failure_requests)
                str_speed = "Seconds/request: " + str(round(elapsed_time/(success_requests+failure_requests),2))
                print(str_elapsed_time, str_success_requests, str_failure_requests, str_speed)
                time_last_status = current_time
                try:
                    progress_recorder.save(self.record_filename)
                except:
                    print ("Can't save last records")

            # si trop de temps s'est ecoule depuis la derniere requete reussie, l'agregation est arretee
            if (current_time - time_last_success >= self.seconds_until_abortion):
                print("Aborting: not enough successful request since" + str(self.seconds_until_abortion) + " seconds")                
                try:
                    progress_recorder.save(self.record_filename)
                except:
                    print ("Can't save last records")
                continue_requesting = False


        file.close()