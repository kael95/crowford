﻿import httplib2
import json
import csv
from time import time
import sys

directory = ""
filename = "campaigns_details"
campaigns_request = "https://www.indiegogo.com/private_api/explore?filter_funding=&filter_percent_funded=&filter_quick=all&filter_status=&per_page=50&pg_num="
attributs_to_save = ['id', 'title', 'category_name', 'cached_collected_pledges_count',
                     'currency_code', 'balance', 'tagline', 'url']

def getElementsFromDict(input_dict, elements_list):
    output_list = []
    for arg in elements_list:
        output_list.append(input_dict[arg])
    return output_list

h = httplib2.Http(".cache")
h.add_credentials('user', 'pass')

file = open(directory + filename + ".csv", 'w', encoding="utf8")
writer = csv.writer(file, delimiter=';', lineterminator='\n', dialect='excel')
writer.writerow(attributs_to_save)

total_campaigns = 0
current_page = 1
continue_requesting = True

starting_time = time()
while continue_requesting is True:
    current_request = campaigns_request + str(current_page)
    #try:
    r, content = h.request(current_request, "GET")
    data = json.loads(content.decode("utf-8"))
    #except Exception:
    #    print("error while looking at page", current_page)
    #    data = []
    if len(data['campaigns']) > 0:
        for campaign in data['campaigns']:
            campaign_attributs = getElementsFromDict(campaign, attributs_to_save)
            writer.writerow(campaign_attributs)
            total_campaigns += 1
    else:
        continue_requesting = False
    print('Page ',current_page,', processed, ',total_campaigns,
          ' campaigns processed in total, during ',round(time() - starting_time),' seconds.')
    current_page += 1
file.close()
