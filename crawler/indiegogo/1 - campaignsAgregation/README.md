* Requirements:
	- Python 3.5
	- The following libraries : httplib2, json, csv

* Usage:
	- Command: python campaignsAgregator.py
	- Exit: Ctrl+C to end the script routine.

* Details:
This script extracts the campaigns main features (such as ID, title, category, balance, url) from the Indiegogo search engine and export those in a CSV file "campaigns_details.csv"

* Warning:
The ouput file is rewritten at every execution of the script.