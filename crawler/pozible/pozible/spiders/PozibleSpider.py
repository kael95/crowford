import scrapy
import re
from pozible.items import ProjectItem, PackageItem, AuthorItem

'''
Command to execute the crawler (windows) from data directory : del output.json && scrapy crawl pozible -o output.json
'''
class PozibleSpider(scrapy.Spider):
    # ----- BEGIN PARAMETERS -----

    # compulsory parameters
    name ="pozible"
    allowed_domains = ["pozible.com"]
    start_urls = [
        'http://www.pozible.com/home/country/us'
    ]

    # because I like hello kitty
    hello_kitty = {
        'main': 'http://www.pozible.com/list/pop/0/all/0/0/{0}',
    }

    # xpath expressions
    xpathexpr = {
        'project': {
            'list': '//div[@class="new_list_article"]/div[@class="new_list_con "]',
            'url_main_page': 'div[@class="new_listcon_bottom"]//h3/a/@href',
            'url_description': '//a[@id="js_description_nav"]/@data-url',
            'title': '//h2[@class="v4_title"]/text()',
            'category': '//ul[@class="v3_nav_ul"]//a[@class="a3"]/text()',
            'date_end': '//p[@class="v4_proinfodiv_p"]/i/text()',
            'location': '//ul[@class="v3_nav_ul"]//a[@class="a1"]/text()',
            'summary': 'div//p//a//text()',
            'money_pledged': 'div[@class="new_listcon_bottom"]/div[@class="new_listcon_pbfcon"]/div[1]//text()',
            'money_percent': 'div[@class="new_listcon_bottom"]/div[@class="new_listcon_pbfcon"]/div[2]//text()',
            'money_goal': '//div[@class="v4_proinfo_float"]/p[@class="v4_proinfo_p"]//text()',
            'nb_updates': '//*[@id="js_pnav"]/a[2]/em/i/text()',
            'nb_supporters': '//*[@id="js_pnav"]/a[3]/em/i/text()',
            'nb_comments': '//*[@id="js_pnav"]/a[4]/em/i/text()',
            'fulldesc': '//*[@id="main_description"]//text()',
            'images': '//*[@id="main_description"]//img'
        },
        'package': {
            'list': '//div[@class="v3_conmain_r pro_rheight"]/div',
            'delivery_infos': 'p[@class="right_coninfo"]/span/text()',
            'price': 'h3/span//text()'
        } ,
        'author': {
            'list': '//p[@class="v4_title_p"]/a/@href',
            'campaigns': {
                'list': '//div[@class="profile_list_con masonry-brick"]',
                'percent_funded': 'div[@class="profile_listcon_bottom"]/div[@class="profile_list_pbfcon"]//text()',
                'funding_unsuccessful': 'div[@class="profile_listcon_bottom"]/div[@class="profile_list_pbfcon"]//span/text()'
            }
        }
    }

    # regexp
    regexpr = {
        'price': r'.{1}(\d+[,]?(\d+)?).?',
        'product_id': '^.+\/(\d+)?$',
        'profile_id': '(.+)\/profile\/(\d+)',
        'percent_funded': '(\d+)%'
    }

    # utils
    def get_xpath_capt(self, result, id_capture):
        ret = '?'
        if (result is not None and len(result) > id_capture):
            ret = result[id_capture].extract()
        return (ret)

    nbErrors = 0
    # ----- END PARAMETERS -----

    # set the language to US because this is a nice way to bullshit
    def parse(self, response):
        return scrapy.Request(self.hello_kitty['main'], callback=self.parse_pages)

    def closed(self, reason):
        print "####### Number of errors: " + str(self.nbErrors)

    # @see parse
    def parse_pages(self, response):
        i = 0
        while i <= 9765:
            yield scrapy.Request(self.hello_kitty['main'].format(i), callback=self.parse_project_list)
            i += 15

    # @see parse_pages
    def parse_project_list(self, response):
        projects = response.xpath(self.xpathexpr['project']['list'])
        for i, project in enumerate(projects):
            try:
                project_item = self.get_project_infos_from_list(project)
            except:
                nbErrors += 1
                continue
            yield scrapy.Request(
                project_item['url_main_page'], callback=self.parse_project_details,
                meta={'project': project_item}
            )

    # @see parse_project_list
    def get_project_infos_from_list(self, project):
        project_item = ProjectItem()
        project_item['url_main_page'] = project.xpath(self.xpathexpr['project']['url_main_page'])[0].extract()
        project_item['sid'] = 3
        project_item['pid'] = (re.search(self.regexpr['product_id'], project_item['url_main_page'])).group(1)
        project_item['summary'] = project.xpath(self.xpathexpr['project']['summary'])[0].extract().encode('utf-8').strip(' \t\n\r')
        money_pledged = project.xpath(self.xpathexpr['project']['money_pledged'])[1].re(r'(.{1})(\d+[,]?\d+).?')
        if (money_pledged is not None and len(money_pledged) >= 2):
            project_item['currency'] = money_pledged[0]
            project_item['money_pledged'] = money_pledged[1]
        project_item['money_percent'] = self.get_xpath_capt(project.xpath(self.xpathexpr['project']['money_percent']), 1)

        return project_item
    
    # @see parse_project_list
    def parse_project_details(self, response):
        project = self.get_project_infos_from_item(response)
        project['packages'] = self.get_packages_infos_from_item(response)

        url_list = []
        url_list.append(response.xpath(self.xpathexpr['project']['url_description'])[0].extract())
        project['authors'] = []
        for author_profile in response.xpath(self.xpathexpr['author']['list']).extract():
            url_list.append(author_profile + '/2')
            url_list.append(author_profile + '/3')
       
        return scrapy.Request(
            url_list[0], callback=self.fetch_project_data_from_urls, 
            meta={'project': project, 'urls': url_list[1:]}
        )

    # @see parse_project_details
    def get_project_infos_from_item(self, response):
        project = response.meta['project']
        project['title'] = self.get_xpath_capt(response.xpath(self.xpathexpr['project']['title']), 0)
        project['category'] = self.get_xpath_capt(response.xpath(self.xpathexpr['project']['category']), 0)
        project['date_end'] = self.get_xpath_capt(response.xpath(self.xpathexpr['project']['date_end']), 0)
        project['location'] = self.get_xpath_capt(response.xpath(self.xpathexpr['project']['location']), 0)
        try:
            project['money_goal'] = response.xpath(self.xpathexpr['project']['money_goal'])[2].re(self.regexpr['price'])[0]
        except:
            project['money_goal'] = '?'
        project['nb_updates'] = self.get_xpath_capt(response.xpath(self.xpathexpr['project']['nb_updates']), 0)
        project['nb_supporters'] = self.get_xpath_capt(response.xpath(self.xpathexpr['project']['nb_supporters']), 0)
        project['nb_comments'] = self.get_xpath_capt(response.xpath(self.xpathexpr['project']['nb_comments']), 0)
        return project

    # @see parse_project_details
    def get_packages_infos_from_item(self, response):
        res_packages = response.xpath(self.xpathexpr['package']['list'])
        packages = []
        for i, package in enumerate(res_packages):
            pkg_item = PackageItem()
            delivery_infos = package.xpath(self.xpathexpr['package']['delivery_infos']).re(r'(\d+)')
            if (delivery_infos is not None and len(delivery_infos) < 2): continue
            pkg_item['nb_sold'] = delivery_infos[0]
            pkg_item['nb_max'] = (int(pkg_item['nb_sold']) + int(delivery_infos[1]) if len(delivery_infos) == 3 else 0)
            pkg_item['price'] = package.xpath(self.xpathexpr['package']['price']).re(self.regexpr['price'])[0]
            packages.append(pkg_item)
        return packages

    # @see parse_project_details
    def fetch_project_data_from_urls(self, response):
        project = response.meta['project']
        url_list = response.meta['urls']
        self.populate_project_with_data_fetched(response, project)
        return (
            project if not url_list
            else scrapy.Request(
                url_list[0], callback=self.fetch_project_data_from_urls,
                meta={'project': project, 'urls': url_list[1:]}
            )
        )

    # @see fetch_project_data_from_urls
    def populate_project_with_data_fetched(self, response, project):
        if "description" in response.url:
            self.fetch_project_full_description(response, project)
        elif "profile" in response.url:
            self.fetch_author_details(response, project)

    # @see populate_project_with_data_fetched
    def fetch_project_full_description(self, response, project):
        project['fulldesc'] = ''.join(response.xpath(self.xpathexpr['project']['fulldesc']).extract())
        project['nb_characters'] = len(project['fulldesc'])
        images = response.xpath(self.xpathexpr['project']['images']).extract()
        project['nb_images'] = len(images)

    # @see populate_project_with_data_fetched
    def fetch_author_details(self, response, project):
        if re.compile('(.+)profile\/(\d+)\/2').match(response.url):
            self.fetch_author_general_details(response, project)
            self.fetch_author_campaigns_supported(response, project['authors'][-1])
        elif re.compile('(.+)profile\/(\d+)\/3').match(response.url):
            self.fetch_author_campaigns_created(response, project['authors'][-1])

    # @see fetch_author_details
    def fetch_author_general_details(self, response, project):
        author = AuthorItem()
        author['sid'] = 3
        author['url_profile'] = response.url
        author['uid'] = (re.search(self.regexpr['profile_id'], author['url_profile'])).group(2)
        author['name'] = self.get_xpath_capt(response.xpath('//div[@class="profile_username"]/text()'), 0)
        project['authors'].append(author)

    # @see fetch_author_details
    def fetch_author_campaigns_supported(self, response, author):
        author['campaigns_supported'] = {'successful': 0, 'unsuccessful': 0, 'pending': 0}
        campaigns = response.xpath(self.xpathexpr['author']['campaigns']['list'])
        for i, campaign in enumerate(campaigns):
           self.populate_author_with_campaigns(campaign, author['campaigns_supported'])

    # @see fetch_author_details
    def fetch_author_campaigns_created(self, response, author):
        author['campaigns_created'] = {'successful': 0, 'unsuccessful': 0, 'pending': 0}
        campaigns = response.xpath(self.xpathexpr['author']['campaigns']['list'])
        for i, campaign in enumerate(campaigns):
           self.populate_author_with_campaigns(campaign, author['campaigns_created'])

    # @see fetch_author_campaigns_supported, fetch_author_campaigns_created
    def populate_author_with_campaigns(self, campaign, campaigns):
        percent_funded = campaign.xpath(self.xpathexpr['author']['campaigns']['percent_funded']).re(self.regexpr['percent_funded'])
        funding_unsuccessful = ''.join(campaign.xpath(self.xpathexpr['author']['campaigns']['funding_unsuccessful']).extract())
        if (percent_funded and int(percent_funded[0]) >= 100):
            campaigns['successful'] += 1
        elif ("unsuccessful" in funding_unsuccessful):
            campaigns['unsuccessful'] += 1
        else:
            campaigns['pending'] += 1