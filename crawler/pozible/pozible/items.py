# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class PackageItem(scrapy.Item):
    nb_sold = scrapy.Field()
    nb_max = scrapy.Field()
    price = scrapy.Field()

class AuthorItem(scrapy.Item):
    sid = scrapy.Field()
    uid = scrapy.Field()
    url_profile = scrapy.Field()
    name = scrapy.Field()
    campaigns_supported = scrapy.Field()
    campaigns_created = scrapy.Field()

class ProjectItem(scrapy.Item):
    sid = scrapy.Field() # 1=kickstarter,2=indiegogo,3=pozible
    pid = scrapy.Field()
    url_main_page = scrapy.Field()
    title = scrapy.Field()
    category = scrapy.Field()
    summary = scrapy.Field()
    fulldesc = scrapy.Field()
    currency = scrapy.Field()
    money_goal = scrapy.Field()
    money_pledged = scrapy.Field()
    money_percent = scrapy.Field()
    date_end = scrapy.Field()
    location = scrapy.Field()
    nb_images = scrapy.Field()
    nb_characters = scrapy.Field()
    nb_supporters = scrapy.Field()
    nb_comments = scrapy.Field()
    nb_updates = scrapy.Field()
    nb_packages = scrapy.Field()
    packages = scrapy.Field()
    authors = scrapy.Field()